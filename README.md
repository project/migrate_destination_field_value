# Migrate destination field value

Add a migration destination plugin that loads an entity and
adds a new value for the selected field then saves the entity.

It is useful when your migration want to modify the current entities you have
in your database.

## Usage

Example migration:
```
id: example_migration
label: 'Example migration'
source:
  plugin: embedded_data
  data_rows:
    -
      channel_machine_name: music
      channel_description: Music
    -
      channel_machine_name: movies
      channel_description: Movies
    ids:
      channel_machine_name:
      type: string
  constants:
    entity_type: node
    field_name: field_example
process:
  entity_type: constants/entity_type
  field_name: constants/field_name
  entity_id:
    plugin: entity_lookup
    source: channel_machine_name
    entity_type: node
    value_key: title
    ignore_case: true
  value: channel_description
destination:
  plugin: field_value
```

So basically you need to fill `entity_type`, `field_name`, `entity_id` and
`value` and
that information will be used to load the entity and fill the information.

## Future improvements

* Configure whether to overwrite the field with the value or add to it.
  Currently it just adds.
* Revert: since we store a sha1 of the value is technically possible to revert
  what is saved although rather complicate.
