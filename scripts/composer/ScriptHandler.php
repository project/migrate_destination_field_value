<?php

/**
 * @file
 * Contains \MigrateDestinationFieldValue\composer\ScriptHandler.
 */

namespace MigrateDestinationFieldValue\composer;

use Composer\Script\Event;
use voku\helper\HtmlMin;

class ScriptHandler
{

    /**
     * Generates a description from the README.
     */
    public static function generateDescription(Event $event)
    {
        $html = (new \Parsedown())->parse(file_get_contents('README.md'));
        // Remove first line since it is the name of the module.
        $html = preg_replace('/^.+\n/', '', $html);
        // Replace line breaks with spaces.
        $html = (new HtmlMin())->minify($html);
        file_put_contents('README.html', $html);
    }
}
